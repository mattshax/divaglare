
# ----- TYPE DEFINITIONS

  type file;
  
  type time {
    string month;
    string day;
    string hour;
  }

# ----- APP DEFINITION

  app (file out_tif,file out_fc,file out_glare,file out_dgp,file out,file err) runIteration ( file docker, file execute, file input, file weather, string month, string day, string hour )
  {
    bash @docker "mattshax/diva" @execute @input @weather month day hour @out_tif @out_fc @out_glare @out_dgp stdout=@out stderr=@err;
  }

# ----- EXTERNAL INPUTS
  
  file execute <single_file_mapper; file=arg("execute","RunAndReduceDiva.sh")>;
  
  file docker_run <single_file_mapper; file="docker_run">;
  
  file input <single_file_mapper; file=arg("dva","office.dva")>;
  file weather <single_file_mapper; file=arg("epw","sf.epw")>;
  
  string outpath=arg("out","results");

  time timeset[] = readData(arg("sweep","hours.sweep"));
  
# ----- PARAMETER SETUP & LAUNCH

  int runs=length(timeset);
  tracef("%i Runs in Simulation\n\n",runs);
  
  foreach time in timeset {
  
    string fileid = @strjoin(["glare",time.month,time.day,time.hour],"_");
    
    file out_tif   <single_file_mapper; file=strcat(outpath,"/tif/",fileid,".tif")>;
    file out_fc    <single_file_mapper; file=strcat(outpath,"/fc/",fileid,".tif")>;
    file out_glare <single_file_mapper; file=strcat(outpath,"/glare/",fileid,".tif")>;
    file out_dgp   <single_file_mapper; file=strcat(outpath,"/dgp/",fileid,".txt")>;
    
    file out <single_file_mapper; file=strcat(outpath,"/out/",fileid,".out")>;
    file err <single_file_mapper; file=strcat(outpath,"/err/",fileid,".err")>;
        
    (out_tif,out_fc,out_glare,out_dgp,out,err) = runIteration(docker_run,execute,input,weather,time.month,time.day,time.hour);
    
  }

