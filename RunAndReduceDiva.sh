#!/bin/bash

# @input @weather month day hour @out_tif @out_fc @out_glare @out_dgp stdout=@out stderr=@err

# inputs
input=$1
weather=$2
month=$3
day=$4
hour=$5

# outputs
out_tif=$6
out_fc=$7
out_glare=$8
out_dgp=$9

# run the glare study
mono /opt/diva/bin/glare.exe $input $weather $month $day $hour

# move output files
filename=$(basename $input)
extension=.$(echo $filename | cut -d "." -f 2)
basename=$(basename $filename $extension)

#TODO - add label to image via RunAndReduce script

mv $basename.tif $out_tif
mv $(echo $basename)_fc.tif $out_fc
mv $(echo $basename)_glare.tif $out_glare
mv $(echo $basename)_glare.txt $out_dgp
