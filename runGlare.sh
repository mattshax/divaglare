#! /bin/bash

# DIVA workflow for point-in-time glare analysis between date ranges
# requires pcregrep and docker

echo "Running DIVA Glare Workflow"

# set the home directory and input filename
divafile=$1;echo "divafile: "$divafile
epwfile=$2;echo "epwfile: "$epwfile
daterange=$3;echo "daterange: "$daterange
result_tif=$4;echo "result_tif: "$result_tif
result_fc=$5;echo "result_fc: "$result_fc
result_glare=$6;echo "result_glare: "$result_glare
result_dgp=$7;echo "result_dgp: "$result_dgp
poolid=$8;echo "poolid: "$poolid
uiport=$9;echo "uiport: "$uiport

divafilebase=$(basename $divafile .dat)

# get the repo files swift.conf file and sed swift.conf coaster port - THIS NEEDS WORK!
dir=$(cd $(dirname $0); /bin/pwd);cp $dir/* ./ -R;SED_ARG="-i s/@@PORT@@/$poolid/ swift.conf";eval sed "$SED_ARG"

# generate the available sunlight hours for the specified epw file
mono daylithours.exe $epwfile hours_all.csv
#docker run -it -v $PWD:$PWD -w $PWD "mattshax/diva" "/usr/bin/mono /opt/diva/bin/daylithours.exe $epwfile hours_all.csv" # > /dev/null 2>&1

# from the available hours_all.csv, make a new hours.csv by extracting $daterange
start=$(echo $daterange | cut -f 1 -d ":")
end=$(echo $daterange | cut -f 2 -d ":")
cat hours_all.csv | pcregrep -M "(?s)(?=^$start,).*?(?<=^$end,)" > hours_tmp.csv

# make the hours.csv expanding foreach value in hours_tmp.csv
echo "month day hour" > hours.sweep
while IFS= read -r line; do
  month=$(echo $line | cut -f 1 -d ",")
  day=$(echo $line | cut -f 2 -d ",")
  hrstart=$(echo $line | cut -f 3 -d ",")
  hrend=$(echo $line | cut -f 4 -d ",")
  for i in $(seq $hrstart $hrend); do
    echo $month $day $i >>hours.sweep
  done
done < hours_tmp.csv
rm hours_tmp.csv;rm hours_all.csv

# start the swift job
uiport=3018
swift -ui http:$uiport glare.swift -dva=$divafile -epw=$epwfile -sweep=hours.sweep

# convert the result images to a video - TODO - add label to image via RunAndReduce script
echo "Post-Processing Images"
convert "-delay" 20 results/tif/* $divafilebase.gif
convert "-delay" 20 results/fc/* $(echo $divafilebase)_fc.gif
convert "-delay" 20 results/glare/* $(echo $divafilebase)_glare.gif

mv $divafilebase.gif $result_tif
mv $(echo $divafilebase)_fc.gif $result_fc
mv $(echo $divafilebase)_glare.gif $result_glare

# compile and move glare files
dgpTmp=results/dgp.txt
echo "time,dgp,dgi,ugr,vcp,cgi,Lveil" > $dgpTmp
for f in results/dgp/*.txt; do
  echo -n $(basename $f .txt) >> $dgpTmp
  cat $f | cut -d ":" -f 2 | sed -e 's/\s\+/,/g' | sed 's/.$//' >> $dgpTmp
done

mv results/dgp.txt $result_dgp

echo "DIVA Glare Workflow Complete"
